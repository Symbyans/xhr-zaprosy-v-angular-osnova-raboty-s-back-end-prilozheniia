angular
    .module('PokemonApp')
    .factory('PokemonsService', function($http) {
                // для версии backendless 3.х - норм
                // в мной используемой - бесполезно.
                // Если подключаться к вашему серверу, заменив url и сняв комент с дефолтных заголовков, всё должно работать

                // более точное описание взял https://docs.angularjs.org/api/ng/service/$http 
                // $http.defaults.headers.common =
                // {   'application-id' : '4B730C92-F81E-236B-FFF0-6651FE882800',
                //     'secret-key'     :  'CB6DE86C-6069-86C4-FF1C-9049D5AC9400'  };

            return {

                getPokemons: function() {
                    return $http.get('https://pokeapi.co/api/v2/pokemon/?limit=10');
                },

                getPokemon: function(pokemonId) {
                    return $http.get('https://pokeapi.co/api/v2/pokemon/' + pokemonId);
                },

                createPokemon: function(pokemonData) {
                    // не удалось зарегистрироваться в версии 3.x .. лишь в 4.x .. 
                    //Там же нет удобного обращения к методу посредством прописания id и secret key в заголовках..
                    //Лишь по прямой ссылки.
                    return $http({
                        method: 'POST',
                        url: 'https://api.backendless.com/D0A791EA-5141-7985-FFC8-CFCBEF5D6300/1C8B9B56-4AD9-5BF2-FFA9-2CEB6CF73000/data/Pokemons',
                        data: pokemonData
                    });
                },
                changePokemon: function(pokemonId, pokemonData) {
                    return $http({
                        method: 'PUT',
                        url: 'https://api.backendless.com/D0A791EA-5141-7985-FFC8-CFCBEF5D6300/1C8B9B56-4AD9-5BF2-FFA9-2CEB6CF73000/data/Pokemons/' + pokemonId,
                        data: pokemonData
                    });
                },
                deletePokemon: function(pokemonId) {
                    return $http({
                        method: 'DELETE',
                        url: 'https://api.backendless.com/D0A791EA-5141-7985-FFC8-CFCBEF5D6300/1C8B9B56-4AD9-5BF2-FFA9-2CEB6CF73000/data/Pokemons/' + pokemonId
                    });
                }

            }

        }

    );
