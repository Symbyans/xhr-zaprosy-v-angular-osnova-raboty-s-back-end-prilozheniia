'use strict';

pokemonApp.controller('PokemonListCtrl', function($scope, $q, PokemonsService, BerriesService) {

    $scope.pokemonsAndBerriesLoaded = false;

    var pokemonsResponse = PokemonsService.getPokemons();
    // .then(function(response) {
    //     $scope.pokemons = response.data.results;
    // });
    
    var berriesResponse = BerriesService.getBerries();
    // .then(function(response) {
    //     $scope.berries = response.data.results;
    // });

    
    $q.all([pokemonsResponse, berriesResponse]).then(function(data){
        $scope.pokemons = data[0].data.results;
        $scope.berries = data[1].data.results;
        $scope.pokemonsAndBerriesLoaded = true;
    });



    // PokemonsService.getPokemons().then(function(response) {
    //     $scope.pokemons = response.data.results;

    //     return BerriesService.getBerries()
    // }).then(function(response) {
    //     $scope.berries = response.data.results;
    // });



});
