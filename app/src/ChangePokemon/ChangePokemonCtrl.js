'use strict';

pokemonApp.controller('ChangePokemonCtrl', function($scope, PokemonsService) {

    $scope.rePokemon = {};

    $scope.changePokemon = function(idPokemon, rePokemon) {

        $scope.changeSuccess = false;

        PokemonsService.changePokemon(idPokemon, rePokemon).then(function(response) {

            $scope.rePokemon = {};

            $scope.rePokemonId = response.data.objectId;
            $scope.changeSuccess = true;

        });

    }

});